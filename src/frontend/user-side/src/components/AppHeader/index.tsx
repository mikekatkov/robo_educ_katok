import React from "react";
import { Segment, Header } from "semantic-ui-react";
import { Grid, Icon } from 'semantic-ui-react'
import Drink from "./../Drink";
export default () => (
    <Segment textAlign='center'>
        <Grid >
            <Grid.Row centered>
                <Grid.Column width={5} className="buttons">
                    <Drink name="Moxito Blyat" icon="venus" desc="Лимон, кровь девственницы, кокс."/>
                </Grid.Column>
                <Grid.Column width={5}>
                    <Drink name="Long-Alend" icon="beer" desc="Экстракт Водолаза, пыль."/>
                </Grid.Column>
                <Grid.Column width={5}>
                    <Drink name="Blood Mess" icon="tint" desc="Твои кишки, в будущем."/>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row centered>
                <Grid.Column width={5}>
                    <Drink name="B-52" icon="star" desc="Моча робота, мазут."/>
                </Grid.Column>
                <Grid.Column width={5}>
                    <Drink name="Маргарита нахуй" icon="trash" desc="Пепел поэмы Гоголя. Водка."/>
                </Grid.Column>
                <Grid.Column width={5}>
                    <Drink name="Дайкри" icon="smile outline" desc="Прах тупой пизды."/>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row centered>
                <Grid.Column width={5}>
                <Drink name="Red Bull" icon="heart" desc="Вода из канализации, сахар, ваниль."/>
                </Grid.Column>
                <Grid.Column width={10}>
                    <Drink name="Random" icon="exclamation triangle" desc="Намешаем вам говна"/>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </Segment>
)