
import * as React from "react";
import { List } from 'semantic-ui-react'
import { Button, Icon } from 'semantic-ui-react'
import { Divider } from 'semantic-ui-react'
import { Segment } from 'semantic-ui-react'
import Axios from "axios";
import { URInfo } from "../../models/urinfo";

export type State = { connection: boolean, info: URInfo };

export default class RobotManager extends React.Component<any, State>
{
    state = { connection: false, info: null }
    //handleItemClick = (e, { name, id }) => this.setState({ activeItem: id })

    render() {
        //let data = await Axios.get("/api/v1/info");
        //if(data.status != 200)
        //    this.setState({connection: false})
        //else
        //    this.setState({connection: true})
       // let info = data.data as URInfo;

        return (
            <div>
                <Segment stacked >

                </Segment>
                <Divider />
                <Button icon>
                    <Icon name='add' />
                </Button>
                <Segment stacked>
                    <List divided relaxed>
                        <List.Item>
                            <List.Content floated='right'>
                                <Button icon>
                                    <Icon name='play' color={"green"} />
                                </Button>
                            </List.Content>
                            <List.Icon name='file code outline' size='large' verticalAlign='middle' />
                            <List.Content>
                                <List.Header as='a'>Base/Program1</List.Header>
                                <List.Description as='a'>Updated 10 mins ago</List.Description>
                            </List.Content>
                        </List.Item>
                        <List.Item>
                            <List.Content floated='right'>
                                <Button icon>
                                    <Icon name='play' color={"green"} />
                                </Button>
                            </List.Content>
                            <List.Icon name='file code outline' size='large' verticalAlign='middle' />
                            <List.Content>
                                <List.Header as='a'>Base/Program2</List.Header>
                                <List.Description as='a'>Updated 15 mins ago</List.Description>
                            </List.Content>
                        </List.Item>
                    </List>
                </Segment>
            </div>
        );
    }
}