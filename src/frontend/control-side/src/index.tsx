import * as ReactDOM from "react-dom";
import * as React from "react";
import * as ReactGA from 'react-ga';
import RobotManager from "./components/Robot";
import App from "./app";
ReactGA.initialize('UA-70421575-6');
ReactGA.pageview(`${window.location.pathname}${window.location.search}`);

function DetectReactPage(page)
{
    switch(page)
    {
        case "index": return <App/>;
    }
}
ReactDOM.render(
    <App/>,
    document.getElementById('root') as HTMLElement
);