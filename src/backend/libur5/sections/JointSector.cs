﻿namespace ur5.sections
{
    using System;
    using core;

    public class JointSector
    {
        /// <summary>
        /// uid motor
        /// </summary>
        public readonly JointType _codeId;
        public JointSector(JointType codeID) => _codeId = codeID;

        public int Mode;
        public float MicroTemperature;
        public float MotorTemperature;
        public float Voltage;
        public float JointCurrent;
        public double JointTarger;
        public double JointPosition;
        public float DegreePosition => (float)Math.Round(JointPosition * 180 / Math.PI, 2);
        public double JointSpeed;
    }
}