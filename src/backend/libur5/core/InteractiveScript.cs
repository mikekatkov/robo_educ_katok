﻿namespace ur5.core
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using RC.Framework.Screens;

    public class InteractiveScript
    {
        private readonly Version _version;

        public enum Version
        {
            v1,
            v2,
            v3
        }

        public InteractiveScript(Version version) => _version = version;
        public static CompiledCode Assembly(params string[] args) => new InteractiveScript(Version.v3).Compile(args);


        public CompiledCode Compile(string[] src_lines)
        {
            var b = new StringBuilder();

            b.AppendLine("def step_forward():");
            var a = MotherBoard.defaultAcceleration;
            var s = MotherBoard.defaultSpeed;

            foreach (var line in src_lines)
            {
                if (string.IsNullOrEmpty(line))
                    continue;
                if (line.First() == '#')
                    continue;
                if (line.Contains("sleep"))
                {
                    b.AppendLine($"\t{line}");
                    continue;
                }

                if (line.Contains("nt_move_tool"))
                {
                    MoveTools(line.Replace("nt_move_tool(", "").Replace(")", ""), b);
                    continue;
                }
                if(!new Regex(@"^\[[0-9.E,\s-]+\]$").IsMatch(line))
                    throw new Exception("you are invalid.");
                b.AppendLine($"\tmovej({line},{a},{s},{0},{0})");
            }//
            b.AppendLine("end");
            return b.ToString();
        }

        private void MoveTools(string value, StringBuilder b)
        {
            var driver = File.ReadAllText("./modbus_base.rb")
                .Replace("\r", "")
                .Replace("$POS_CLOSE", value);
            
            foreach (var s in driver.Split('\n'))
                b.AppendLine($"\t{s}");
        }
    }
    public class CompiledCode
    {
        private CompiledCode(string code) => _code = code;
        private string _code { get; set; }

        public static implicit operator string(CompiledCode code) => code._code;
        public static implicit operator CompiledCode(string code) => new CompiledCode(code);
    }
}