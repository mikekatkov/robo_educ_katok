﻿namespace ur5.core
{
    using System.Threading;
    using System.Threading.Tasks;
    using etc;
    using JetBrains.Annotations;
    using models;

    public static class Modbus
    {
        /// <summary>
        /// Modbus event delegate
        /// </summary>
        public delegate void ListenerSet(ModbusInfoMessage message);
        /// <summary>
        /// Sync lock object
        /// </summary>
        public static readonly object SyncGuarder = new object();

        /// <summary>
        /// Listeren event input gate
        /// </summary>
        [UsedImplicitly]
        public static event ListenerSet ListerenGate
        {
            add
            {
                lock (SyncGuarder)
                {
                    _internalModBusGate += value;
                    var len = _internalModBusGate?.GetInvocationList().Length;
                    if (len > 10)
                        LibLog.Warn($"There are now {len} modbus message subscribers.");
                }
            }
            remove
            {
                lock (SyncGuarder)
                {
                    _internalModBusGate -= value;
                }
            }
        }
        /// <summary>
        /// Notifity all subscibes
        /// </summary>
        /// <remarks>
        /// Thread-safe
        /// Non-lock
        /// </remarks>
        [PublicAPI]
        public static void NotifityAll(ModbusInfoMessage message)
        {
            var tokenSource = new CancellationTokenSource();
            var task = new Task(() =>
            {
                lock (SyncGuarder)
                {
                    _internalModBusGate?.Invoke(message);
                }
            }, tokenSource.Token);
            task.Start();
        }

        #region Private
        private static event ListenerSet _internalModBusGate;

        #endregion
    }
}