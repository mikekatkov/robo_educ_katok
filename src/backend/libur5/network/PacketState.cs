﻿namespace ur5.network
{
    using core;

    public class PacketState
    {
        public PacketState(int size) => CurrentMessageLength = size;

        public UR5Info RobotState { get; set; } = new UR5Info();
        public int MessageOffset { get; set; } = 0;
        
        public int CurrentMessageLength { get; set; } = 0;
        public short CurrentMessageOpCode { get; set; } = 0;
    }
}