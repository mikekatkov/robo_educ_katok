﻿namespace ur5.network
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using etc;

    public class LowLevelSocket : IDisposable
    {
        private readonly Encoding _encoding;
        private readonly Socket _socket;
        private bool supressWrite;

        #region Constructors

        /// <summary>
        /// Constructs and connects the socket.
        /// </summary>
        /// <param name="endpoint">Endpoint to connect to</param>
        public LowLevelSocket(EndPoint endpoint) : this(endpoint, Encoding.UTF8) { }

        /// <summary>
        /// Constructs and connects the socket.
        /// </summary>
        public LowLevelSocket(EndPoint endpoint, Encoding encoding)
        {
            _encoding = encoding;
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _socket.Connect(endpoint);
        }

        /// <summary>
        /// Constructs and connects the socket.
        /// </summary>
        public LowLevelSocket(string host, int port) : this(host, port, Encoding.UTF8) { }

        /// <summary>
        /// Constructs and connects the socket.
        /// </summary>
        public LowLevelSocket(string host, int port, Encoding encoding)
        {
            _encoding = encoding;
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _socket.Connect(host, port);
        }

        internal LowLevelSocket(Socket socket)
        {
            _encoding = Encoding.UTF8;
            _socket = socket;
        }

        #endregion

        #region Interfaces

        /// <summary>
        /// Disposes the socket.
        /// </summary>
        public void Dispose()
        {
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Dispose();
        }

        #endregion

        #region Receive

        /// <summary>
        /// Receives any pending data.
        /// This blocks execution until there's data available.
        /// </summary>
        public string Receive(int bufferSize = 1024)
        {
            var buffer = new byte[bufferSize];
            _socket.Receive(buffer);
            return _encoding.GetString(buffer).TrimEnd('\0');
        }
        /// <summary>
        /// Receives any pending data.
        /// This blocks execution until there's data available.
        /// </summary>
        public byte[] ReceiveBytes(int bufferSize = 4096)
        {
            var buffer = new byte[bufferSize];
            _socket.Receive(buffer);
            var data_found = false;
            var array = buffer.Reverse().SkipWhile(point =>
            {
                if (data_found) return false;
                if (point == 0x0) return true;
                data_found = true;
                return false;
            }).Reverse().ToArray();
            return array;
        }

        #endregion

        /// <summary>
        /// Sends the given data.
        /// </summary>
        /// <param name="data">Data to send</param>
        public void Send(string data)
        {
            if (!supressWrite)
            {
                if(!IsConnected())
                    LibLog.Error($"Not connected to controller, trying to send string: {data}");
                _socket.Send(_encoding.GetBytes(data));
            }
        }

        /// <summary>
        /// Socket is connected?
        /// </summary>
        public bool IsConnected()
        {
            if (supressWrite)
                return true;
            return _socket != null && _socket.Connected;
        }

        public void Disconnect()
        {
            if(supressWrite)
                return;
            if(_socket == null)
                return;
            if(!_socket.Connected)
                return;
            _socket.Disconnect(true);
        }
        /// <summary>
        /// Switch supressing write actions
        /// </summary>
        public void SupressWrite() => supressWrite = !supressWrite;
    }
}