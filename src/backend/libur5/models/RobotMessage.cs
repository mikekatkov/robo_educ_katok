﻿namespace ur5.models
{
    using System;
    using JetBrains.Annotations;

    public class RobotMessage
    {
        public long TimeStamp { get; set; }
        public int Source { get; set; }
        public RobotMessageType Type { get; set; } = RobotMessageType.UNDEFINED;
        public int ErrorCode { get; set; } = 0;
        public int ErrorArgument { get; set; } = 0;
        public object ExtraData { get; set; } = null;
        public int ID { get; set; } = -1;
        public string Message { get; set; } = "";
        public string Title { get; set; } = "";

        public override string ToString()
        {
            switch (Type)
            {
                case RobotMessageType.VERSION:
                case RobotMessageType.TEXT:
                    return this.Message;
                case RobotMessageType.PROGRAM_LABEL:
                    return $"{this.Message} {ID}";
                case RobotMessageType.UNDEFINED:
                    return "undefined";
                case RobotMessageType.ROBOTCOMM:
                case RobotMessageType.SECURITY:
                case RobotMessageType.REQUEST_VALUE:
                    return SelectString(this);
                case RobotMessageType.MESSAGE_KEY:
                    return $"{Title} ";
                case RobotMessageType.VARIABLE:
                    return $"{Title}={Message}";
                case RobotMessageType.POPUP:
                default:
                    return $"ILLEGAL ROBOT MESSAGE TYPE: {Type}";
            }


        }

        public static string SelectString(RobotMessage msg)
        {
            return msg.ID.ToString();
        }

        public static void Notifity(RobotMessage msg)
        {
            Console.WriteLine($"[{msg.Type}][{msg.ID}][{msg.Title}]: {msg.Message}");
        }
    }

    [UsedImplicitly, PublicAPI]
    public enum RobotMessageType
    {
        TEXT = 0,
        PROGRAM_LABEL,
        POPUP,
        VERSION,
        /// <summary>
        /// ahaha, ur-controller written in javascript)0))
        /// </summary>
        UNDEFINED,
        SECURITY,
        ROBOTCOMM,
        MESSAGE_KEY,
        VARIABLE,
        REQUEST_VALUE
    }
}