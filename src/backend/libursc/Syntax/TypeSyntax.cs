﻿namespace urscript.interactive
{
    using System.Collections.Generic;
    using System.Linq;

    public class TypeSyntax
    {
        private List<string> Namespaces { get; }
        private string Identifier { get; }
        public TypeSyntax(IEnumerable<string> qualifiedName)
        {
            Namespaces = qualifiedName.ToList();

            if (!Namespaces.Any()) return;

            var lastItem = Namespaces.Count - 1;
            Identifier = Namespaces[lastItem];
            Namespaces.RemoveAt(lastItem);
        }

        public TypeSyntax(params string[] qualifiedName)
            : this(qualifiedName.AsEnumerable())
        {
        }


        public string AsString() =>
            string.Join(".", Namespaces.Concat(Enumerable.Repeat(Identifier, 1)));
    }
}