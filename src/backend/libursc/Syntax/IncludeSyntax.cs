﻿namespace urscript.interactive
{
    using System.Collections.Generic;

    public class IncludeSyntax
    {
        public IEnumerable<string> FilesName { get; set; }
    }
}