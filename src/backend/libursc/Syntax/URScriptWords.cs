﻿namespace urscript.interactive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class URScriptWords
    {

        // System Types
        public const string Void = "void";
        public const string Gate = "gate";
        public const string Fragment = "Fragment";
        public const string Linear = "Linear";
        public const string Joint = "Joint";
        public const string Circular = "Circular";


        // URTypes
        public const string Vector = "vector";
        public const string Point = "point";
        public const string Position = "position";
        public const string Speed = "speed";
        public const string Acceleration = "acc";
        public const string Rotation = "rotation";

        // System words

        public const string When = "when";
        public const string Include = "include";
        public const string As = "as";
        



        #region Etc

        public static HashSet<string> ReservedWords { get; } =
            GetStrings(AllStringConstants.ToArray());
        private static HashSet<string> GetStrings(params string[] strings) =>
            new HashSet<string>(strings, StringComparer.OrdinalIgnoreCase);
        private static IEnumerable<string> AllStringConstants =>
            typeof(URScriptWords).GetTypeInfo().GetFields().Select(f => f.GetValue(null)).OfType<string>();

        #endregion
    }
}