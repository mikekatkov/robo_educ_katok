﻿namespace urscript.interactive
{
    using System;
    public class InfusedModuleRecursionException : Exception
    {
        private readonly string _module1;

        public InfusedModuleRecursionException(string module1) => _module1 = module1;

        public override string Message =>
            $"Module '{_module1}' has recursive import.";
    }
}