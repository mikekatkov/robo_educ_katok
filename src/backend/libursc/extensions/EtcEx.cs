﻿namespace urscript.interactive.extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class EtcEx
    {
        public static IEnumerable<string> Dry(this IEnumerable<string> enur) => enur.Where(x => !string.IsNullOrEmpty(x));
    }
}