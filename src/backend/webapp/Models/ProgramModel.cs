﻿namespace backend.Models
{
    using System;

    public class ProgramModel
    {
        public int ID { get; set; }

        public string Folder { get; set; }
        public string Name { get; set; }
        public DateTime LastEdit { get; set; }
    }
}